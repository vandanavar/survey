from django.conf.urls import url
from django.urls import path
from . import views

urlpatterns = [
    url(r'^$', views.home, name='home'),
    path('<value>', views.bsv_view,name='ac')

    # url(r'^(?P<pk>[0-9]+)/$', views.datePick, name='pickup'),
    # url(r'^register/$', views.register, name='register'),
    # url(r'^login/$', views.user_login, name='login'),
    # url(r'^logout/$', views.user_logout, name='logout'),
    # url(r'^car_list/(?P<pk>[0-9]+)/$', views.carList.as_view(), name='car_list'),
    #url(r'^pick_up$', views.datePick, name='date_pick'),
    #<a href="{% url 'pickup' location.id%}">{{ location.location_name }}</a>
]