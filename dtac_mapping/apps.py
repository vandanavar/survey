from django.apps import AppConfig


class DtacMappingConfig(AppConfig):
    name = 'dtac_mapping'
