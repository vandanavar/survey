from rest_framework import serializers
from .models import BsvDistrictAcMapping

class BsvSerializer(serializers.ModelSerializer):
    class Meta:
        model=BsvDistrictAcMapping
        fields=['id','district','ac']