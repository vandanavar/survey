from django.shortcuts import render
from .models import BsvDistrictAcMapping
from rest_framework import status
from rest_framework.response import Response
from rest_framework.decorators import api_view
from .serializers import BsvSerializer

# Create your views here.
def home(request):
    districts=BsvDistrictAcMapping.objects.values('district').distinct()
    return render(request, 'home.html', {'districts':districts})

@api_view(['GET',])
def bsv_view(request,value):
    acs=BsvDistrictAcMapping.objects.filter(district=value)

    if request.method == "GET":
        serial=BsvSerializer(acs,many='true')
        print(serial.data)
        return Response(serial.data)
