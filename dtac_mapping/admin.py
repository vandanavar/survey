from django.contrib import admin

# Register your models here.
from .models import BsvDistrictAcMapping

admin.site.register(BsvDistrictAcMapping)
